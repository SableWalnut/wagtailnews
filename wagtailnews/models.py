from __future__ import absolute_import, unicode_literals

from six import text_type, string_types

from django.conf.urls import url
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.utils import timezone
from django.utils.text import slugify

from wagtail.contrib.wagtailroutablepage.models import RoutablePageMixin, route
from wagtail.wagtailadmin.edit_handlers import FieldPanel
from wagtail.wagtailcore.models import Page
from wagtail.wagtailcore.utils import resolve_model_string
from wagtail.wagtailsearch import index


NEWSINDEX_MODEL_CLASSES = []
_NEWSINDEX_CONTENT_TYPES = []


def get_newsindex_content_types():
    global _NEWSINDEX_CONTENT_TYPES
    if len(_NEWSINDEX_CONTENT_TYPES) != len(NEWSINDEX_MODEL_CLASSES):
        _NEWSINDEX_CONTENT_TYPES = [
            ContentType.objects.get_for_model(cls)
            for cls in NEWSINDEX_MODEL_CLASSES]
    return _NEWSINDEX_CONTENT_TYPES


class NewsIndexMixin(RoutablePageMixin):

    class Meta:
        pass

    newsitem_model = None
    subpage_types = []

    @route(r'^$', name='index')
    def v_index(s, r):
        return frontend.news_index(r, s)

    @route(r'^(?P<year>\d{4})/$', name='year')
    def v_year(s, r, **k):
        return frontend.news_year(r, s, **k)

    @route(r'^(?P<year>\d{4})/(?P<month>\d{1,2})/$', name='month')
    def v_month(s, r, **k):
        return frontend.news_month(r, s, **k)

    @route(r'^(?P<year>\d{4})/(?P<month>\d{1,2})/(?P<day>\d{1,2})/$', name='day')
    def v_day(s, r, **k):
        return frontend.news_day(r, s, **k)

    @route(r'^(?P<year>\d{4})/(?P<month>\d{1,2})/(?P<day>\d{1,2})/(?P<pk>\d+)-(?P<slug>.*)/$', name='post')
    def v_post(s, r, **k):
        return frontend.newsitem_detail(r, s, **k)

    @classmethod
    def get_newsitem_model(cls):
        if isinstance(cls.newsitem_model, models.Model):
            return cls.newsitem_model
        elif isinstance(cls.newsitem_model, string_types):
            return resolve_model_string(cls.newsitem_model, cls._meta.app_label)
        else:
            raise ValueError('Can not resolve {0}.newsitem_model in to a model: {1!r}'.format(
                cls.__name__, cls.newsitem_model))


class AbstractNewsItem(models.Model):

    newsindex = models.ForeignKey(Page)
    time = models.DateTimeField('Published date', default=timezone.now, help_text="Not viewable by public")

    panels = [
        FieldPanel('time'),
    ]

    search_fields = (index.FilterField('time'),)

    class Meta:
        ordering = ('-time',)
        abstract = True

    def get_nice_url(self):
        return slugify(text_type(self))

    def get_template(self, request):
        try:
            return self.template
        except AttributeError:
            return '{0}/{1}.html'.format(self._meta.app_label, self._meta.model_name)

    def url(self):
        newsindex = self.newsindex.specific
        ldate = timezone.localtime(self.date)
        url = newsindex.url + newsindex.reverse_subpage('post', kwargs={
            'year': ldate.year, 'month': ldate.month, 'day': ldate.day,
            'pk': self.pk, 'slug': self.get_nice_url()})
        return url


# Need to import this down here to prevent circular imports :(
from .views import frontend
